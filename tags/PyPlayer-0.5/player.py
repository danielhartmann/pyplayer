# -*- coding: utf8 -*-

__author__ = "Daniel Hartmann"
__date__ = "$15/03/2011 21:44:22$"

# QT Stuff
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from interface import *
from interface_preferences import Ui_SettingsDialog as SettingsDialog

# GStreamer Stuff
import pygst
pygst.require("0.10")
import gst

# MP3 ID3 Tag Reading
from mutagen.easyid3 import EasyID3
from mutagen.mp3 import MP3

import collections

# configuration file handling
import config

# to control song slider
import threading
import time

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class control(threading.Thread):
    running = False

    def __init__(self, ui):
        self.ui = ui
        threading.Thread.__init__ (self)

    def run(self):
        while 1:
            if self.running:
                time.sleep(1)
                print 'MOVE'

class player:
    PLAYING = 'playing'
    PAUSED = 'paused'
    STOPPED = 'stopped'

    INITIAL_VOLUME = 0.7

    currentsong = "lets_dance.mp3"
    currentposition = 0

    def __init__(self):
        # creates a gstreamer pipeline
        self.pipeline = gst.Pipeline("player-pipeline-dh")

        # sets pipeline properties
        self.filesrc = gst.element_factory_make("filesrc", "audio")
        self.filesrc.set_property("location", self.currentsong)

        # to support more file formats, this must be changed
        self.decoder = gst.element_factory_make("mad", "mp3-decoder")

        self.audioconvert = gst.element_factory_make("audioconvert")
        self.audioresample = gst.element_factory_make("audioresample")

        self.volume = gst.element_factory_make('volume')
        self.volume.set_property('volume', self.INITIAL_VOLUME)
        self.volume.set_property('mute', False)

        # TODO: add some more bands to equalizer
        # equalizers: equalizer-3bands, equalizer-10bands, equalizer-nbands
        self.equalizer = gst.element_factory_make('equalizer-3bands')

        self.sink = gst.element_factory_make("alsasink", "sink")
        self.pipeline.add(
            self.filesrc,
            self.decoder,
            self.audioconvert,
            self.audioresample,
            self.equalizer,
            self.volume,
            self.sink
        )
        gst.element_link_many(
            self.filesrc,
            self.decoder,
            self.audioconvert,
            self.audioresample,
            self.equalizer,
            self.volume,
            self.sink
        )

    def play(self):
        self.filesrc.set_property("location", self.currentsong)
        if self.pipeline.get_state()[1] == gst.STATE_PLAYING:
            self.pipeline.set_state(gst.STATE_PAUSED)
            return self.PAUSED
        else:
            self.pipeline.set_state(gst.STATE_PLAYING)
            return self.PLAYING

    def stop(self):
        self.pipeline.set_state(gst.STATE_READY)
        self.currentposition = 0

    def changeVolume(self, value):
        self.volume.set_property('volume', value)

    def bandNChanged(self, bandNumber, value):
        self.equalizer.set_property( "band"+str(bandNumber), float( value ) )

    def getMP3Data(self, songname=None):
        if not songname:
            songname = self.currentsong

        # id3 data
        song = EasyID3(songname)
        # audio data
        audio = MP3(songname)

        songdata = collections.defaultdict(lambda: '')
        for field in song:
            songdata[field] = song[field][0]

        songdata['bitrate'] = audio.info.bitrate
        songdata['length'] = audio.info.length

        return songdata

    def changePosition(self, value):
        self.pipeline.seek_simple(gst.FORMAT_TIME, gst.SEEK_FLAG_FLUSH | gst.SEEK_FLAG_KEY_UNIT, value * gst.SECOND)
        self.currentposition = value

class playerInterface(Ui_MainWindow):

    def initPlayer(self):
        self.player = player()

        list = config.config.setdefault('playlist', [])

        rowscount = len(list)
        columnscount = 6 # fixed

        # initialize interface
        self.playlist.setRowCount(rowscount) # FIXME
        self.playlist.setColumnCount(columnscount)

        for x in range(0, rowscount):
            for y in range(0, columnscount):
                self.playlist.setItem(x, y, QtGui.QTableWidgetItem())

        for row, song in enumerate(list):
            songdata = self.player.getMP3Data(song)
            self.playlist.item(row, 0).setText(songdata['tracknumber'])
            self.playlist.item(row, 1).setText(songdata['title'])
            self.playlist.item(row, 2).setText(songdata['artist'])
            self.playlist.item(row, 3).setText(songdata['album'])
            self.playlist.item(row, 4).setText(songdata['year'])
            self.playlist.item(row, 5).setText(song)

        if rowscount > 0:
            self.player.currentsong = list[0]

            length = self.player.getMP3Data(list[0])['length']
            self.durationSlider.setMaximum(int(length*100))

            #self.timeLabel.setText('0:00 / ' + str(length))

        # connect play on enter event
        self.playlist.doubleClicked.connect(ui.playSelectedRow)


    def openSettingsDialog(self):
        Dialog = QDialog(self.centralwidget)
        self.dialog = SettingsDialog()
        self.dialog.setupUi(Dialog)
        Dialog.show()

    def togglePlaylistVisibility(self):
        if self.playlistWidget.isHidden():
            self.playlistWidget.show()
            height = self.MainWindow.height() + self.playlistWidget.height()
            self.MainWindow.resize(self.MainWindow.width(), height)
        else:
            height = self.MainWindow.height() - self.playlistWidget.height()
            self.playlistWidget.hide()
            self.MainWindow.resize(self.MainWindow.width(), height)

    def playerPlay(self):
        
        playerstate = self.player.play()

        if playerstate == player.PLAYING:

            control.running = True

            self.playButton.setText('Pause')

            artist = self.player.getMP3Data()['artist']
            self.artistname.setText(_fromUtf8(artist))

            song = self.player.getMP3Data()['title']
            self.songtitle.setText(_fromUtf8(song))

        elif playerstate == player.PAUSED:
            self.playButton.setText('Play')

    def durationChanged(self, value):
        self.player.changePosition(float(value)/float(100))

    def volumeMuted(self):
        if self.player.volume.get_property('mute'):
            self.player.volume.set_property('mute', False)
            self.volumeLabel.setStyleSheet(_fromUtf8("image: url(\'../imgs/sound.png\')"))
        else:
            self.player.volume.set_property('mute', True)
            self.volumeLabel.setStyleSheet(_fromUtf8("image: url(\'../imgs/mute.png\')"))

    def volumeChanged(self, value):
        volume = float(value)/100
        self.player.changeVolume(volume)

    def playerBand0changed(self, value):
        self.player.bandNChanged(0, value)

    def playerBand1changed(self, value):
        self.player.bandNChanged(1, value)

    def playerBand2changed(self, value):
        self.player.bandNChanged(2, value)

    def addSongDialog(self):
        filedialog = QFileDialog(self.centralwidget)
        filedialog.connect(filedialog, SIGNAL(_fromUtf8("fileSelected(QString)")), self.addSong)
        filedialog.show()

    def addSong(self, path, save=True):
        print path
        nextRow = self.playlist.rowCount()
        self.playlist.setRowCount(self.playlist.rowCount() + 1)

        for y in range(0, self.playlist.columnCount()):
            self.playlist.setItem(nextRow, y, QtGui.QTableWidgetItem())

        if save:
            config.addsong(path)

        songdata = self.player.getMP3Data(path)

        self.playlist.item(nextRow, 0).setText(songdata['tracknumber'])
        self.playlist.item(nextRow, 1).setText(songdata['title'])
        self.playlist.item(nextRow, 2).setText(songdata['artist'])
        self.playlist.item(nextRow, 3).setText(songdata['album'])
        self.playlist.item(nextRow, 4).setText(songdata['year'])
        self.playlist.item(nextRow, 5).setText(path)

    def removeSong(self):
        rows = sorted(set([(item.row(), item.text()) for item in self.playlist.selectedItems() if item.column() == 5]), reverse=True)
        for row, song in rows:
            self.playlist.removeRow(row)
            config.removesong(song)
        config.save()

    def playSelectedRow(self):
        items = self.playlist.selectedItems()

        playerstate = self.player.stop()
        self.player.currentsong = items[5].text()
        playerstate = self.player.play()

        self.playButton.setText('Pause')

        artist = self.player.getMP3Data()['artist']
        self.artistname.setText(_fromUtf8(artist))

        song = self.player.getMP3Data()['title']
        self.songtitle.setText(_fromUtf8(song))

        self.durationSlider.setMaximum(int(self.player.getMP3Data()['length']*100))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = playerInterface()
    ui.setupUi(MainWindow)
    ui.MainWindow = MainWindow

    # initialize the player attribute
    ui.initPlayer()

    # enable mouse events on qlabel
    ui.volumeLabel.setMouseTracking(True)

    # set initial volume slider value
    ui.volumeSlider.setValue(100 * player.INITIAL_VOLUME)
    ui.volumeSlider.setSliderPosition(100 * player.INITIAL_VOLUME)

    # TODO: rotate equalizer labels

    # connect signals between qt class and player interface
    QtCore.QObject.connect(ui.playButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.playerPlay)
    QtCore.QObject.connect(ui.settingsButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.openSettingsDialog)
    QtCore.QObject.connect(ui.showPlaylistButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.togglePlaylistVisibility)

    QtCore.QObject.connect(ui.addButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.addSongDialog)
    QtCore.QObject.connect(ui.removeButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.removeSong)

    # connect volume controls
    def volumeEvent(label): ui.volumeMuted()
    ui.volumeLabel.mousePressEvent = volumeEvent
    QtCore.QObject.connect(ui.volumeSlider, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), ui.volumeChanged)

    # connect equalizer sliders
    QtCore.QObject.connect(ui.band0, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), ui.playerBand0changed)
    QtCore.QObject.connect(ui.band1, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), ui.playerBand1changed)
    QtCore.QObject.connect(ui.band2, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), ui.playerBand2changed)

    # connect duration slider
    QtCore.QObject.connect(ui.durationSlider, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), ui.durationChanged)

    # thread to control song position slider
    #c = control(ui)
    #c.start()

    MainWindow.show()

    sys.exit(app.exec_())