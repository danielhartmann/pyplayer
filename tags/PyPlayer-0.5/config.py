
import os
import yaml

PROJECT_NAME = 'pyplayer'
HOME_DIR = os.path.expanduser('~')
CONFIG_DIR = os.path.join(HOME_DIR, '.config', PROJECT_NAME)
CONFIG_FILE = os.path.join(CONFIG_DIR, 'playlist.yaml')

try:
    os.mkdir(CONFIG_DIR)
except:
    pass


file = open(CONFIG_FILE, 'r+')
config = yaml.load(file.read())

if config is None:
    config = {}

def save():
    open(CONFIG_FILE, 'w').write(yaml.dump(config))

def addsong(path):
    try:
        config['playlist'].append(path)
    except:
        config['playlist'] = []
        config['playlist'].append(path)
    save()

def removesong(path):
    try:
        config['playlist'].remove(path)
    except:
        pass
