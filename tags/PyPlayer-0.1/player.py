# -*- coding: utf8 -*-

__author__ = "Daniel Hartmann"
__date__ = "$15/03/2011 21:44:22$"

# QT Stuff
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from interface import *
from interface_preferences import Ui_SettingsDialog as SettingsDialog

# GStreamer Stuff
import pygst
pygst.require("0.10")
import gst

# MP3 ID3 Tag Reading
from mutagen.easyid3 import EasyID3

import collections

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


class player:
    PLAYING = 'playing'
    PAUSED = 'paused'
    STOPPED = 'stopped'

    currentsong = "lets_dance.mp3"

    def __init__(self):
        # creates a gstreamer pipeline
        self.pipeline = gst.Pipeline("player-pipeline-dh")

        # sets pipeline properties
        self.filesrc = gst.element_factory_make("filesrc", "audio")
        self.filesrc.set_property("location", self.currentsong)
        self.decoder = gst.element_factory_make("mad", "mp3-decoder")
        self.audioconvert = gst.element_factory_make("audioconvert")
        self.audioresample = gst.element_factory_make("audioresample")

        # TODO: add some more bands to equalizer
        # equalizers: equalizer-3bands, equalizer-10bands, equalizer-nbands
        self.equalizer = gst.element_factory_make('equalizer-3bands')

        self.sink = gst.element_factory_make("alsasink", "sink")
        self.pipeline.add(self.filesrc, self.decoder, self.audioconvert,
                          self.audioresample, self.equalizer, self.sink)
        gst.element_link_many(self.filesrc, self.decoder, self.audioconvert,
                              self.audioresample, self.equalizer, self.sink)

    def play(self):
        self.filesrc.set_property("location", self.currentsong)
        if self.pipeline.get_state()[1] == gst.STATE_PLAYING:
            self.pipeline.set_state(gst.STATE_PAUSED)
            return self.PAUSED
        else:
            self.pipeline.set_state(gst.STATE_PLAYING)
            return self.PLAYING

    def stop(self):
        self.pipeline.set_state(gst.STATE_READY)

    def bandNChanged(self, bandNumber, value):
        self.equalizer.set_property( "band"+str(bandNumber), float( value ) )

    def getMP3Data(self, songname=None):
        if songname:
            song = EasyID3(songname)
        else:
            song = EasyID3(self.currentsong)

        songdata = collections.defaultdict(lambda: '')
        for field in song:
            songdata[field] = song[field][0]

        return songdata


class playerInterface(Ui_MainWindow):

    def initPlayer(self):
        self.player = player()

        rowscount = 1
        columnscount = 6 # fixed

        # initialize interface
        self.playlist.setRowCount(rowscount) # FIXME
        self.playlist.setColumnCount(columnscount)

        for x in range(0, rowscount):
            for y in range(0, columnscount):
                self.playlist.setItem(x, y, QtGui.QTableWidgetItem())

        if self.player.currentsong:
            songdata = self.player.getMP3Data()
            self.playlist.item(0, 0).setText(songdata['track'])
            self.playlist.item(0, 1).setText(songdata['title'])
            self.playlist.item(0, 2).setText(songdata['artist'])
            self.playlist.item(0, 3).setText(songdata['album'])
            self.playlist.item(0, 4).setText(songdata['year'])
            self.playlist.item(0, 5).setText(self.player.currentsong)

        # connect play on enter event
        self.playlist.doubleClicked.connect(ui.playSelectedRow)

    def openSettingsDialog(self):
        Dialog = QDialog(self.centralwidget)
        self.dialog = SettingsDialog()
        self.dialog.setupUi(Dialog)
        Dialog.show()

    def togglePlaylistVisibility(self):
        if self.playlistWidget.isHidden():
            self.playlistWidget.show()
            height = self.MainWindow.height() + self.playlistWidget.height()
            self.MainWindow.resize(self.MainWindow.width(), height)
        else:
            height = self.MainWindow.height() - self.playlistWidget.height()
            self.playlistWidget.hide()
            self.MainWindow.resize(self.MainWindow.width(), height)

    def playerPlay(self):
        
        playerstate = self.player.play()

        if playerstate == player.PLAYING:
            self.playButton.setText('Pause')

            artist = self.player.getMP3Data()['artist']
            self.artistname.setText(_fromUtf8(artist))

            song = self.player.getMP3Data()['title']
            self.songtitle.setText(_fromUtf8(song))

        elif playerstate == player.PAUSED:
            self.playButton.setText('Play')


    def playerBand0changed(self, value):
        self.player.bandNChanged(0, value)

    def playerBand1changed(self, value):
        self.player.bandNChanged(1, value)

    def playerBand2changed(self, value):
        self.player.bandNChanged(2, value)

    def addSongDialog(self):
        filedialog = QFileDialog(self.centralwidget)
        filedialog.connect(filedialog, SIGNAL(_fromUtf8("fileSelected(QString)")), self.addSong)
        filedialog.show()

    def addSong(self, path):
        print path
        nextRow = self.playlist.rowCount()
        self.playlist.setRowCount(self.playlist.rowCount() + 1)

        for y in range(0, self.playlist.columnCount()):
            self.playlist.setItem(nextRow, y, QtGui.QTableWidgetItem())

        songdata = self.player.getMP3Data(path)

        self.playlist.item(nextRow, 0).setText(songdata['track'])
        self.playlist.item(nextRow, 1).setText(songdata['title'])
        self.playlist.item(nextRow, 2).setText(songdata['artist'])
        self.playlist.item(nextRow, 3).setText(songdata['album'])
        self.playlist.item(nextRow, 4).setText(songdata['year'])
        self.playlist.item(nextRow, 5).setText(path)

    def removeSong(self):
        rows = sorted(set([item.row() for item in self.playlist.selectedItems()]), reverse=True)
        for row in rows:
            self.playlist.removeRow(row)

    def playSelectedRow(self):
        items = self.playlist.selectedItems()

        playerstate = self.player.stop()
        self.player.currentsong = items[5].text()
        playerstate = self.player.play()


        self.playButton.setText('Pause')

        artist = self.player.getMP3Data()['artist']
        self.artistname.setText(_fromUtf8(artist))

        song = self.player.getMP3Data()['title']
        self.songtitle.setText(_fromUtf8(song))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = playerInterface()
    ui.setupUi(MainWindow)
    ui.MainWindow = MainWindow

    # initialize the player attribute
    ui.initPlayer()

    # connect signals between qt class and player interface
    QtCore.QObject.connect(ui.playButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.playerPlay)
    QtCore.QObject.connect(ui.settingsButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.openSettingsDialog)
    QtCore.QObject.connect(ui.showPlaylistButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.togglePlaylistVisibility)

    QtCore.QObject.connect(ui.addButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.addSongDialog)
    QtCore.QObject.connect(ui.removeButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.removeSong)

    QtCore.QObject.connect(ui.band0, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), ui.playerBand0changed)
    QtCore.QObject.connect(ui.band1, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), ui.playerBand1changed)
    QtCore.QObject.connect(ui.band2, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), ui.playerBand2changed)

    MainWindow.show()

    sys.exit(app.exec_())

