
# http://initd.org/psycopg/docs/usage.html

import psycopg2

class db:
    def __init__(self):
        self.connection = psycopg2.connect("dbname=player user=postgres")
        self.cursor = self.connection.cursor()

    def __del__(self):
        self.connection.close()

    def query(self, sql):
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def execute(self, sql):
        self.cursor.execute(sql)
        self.connection.commit()

class music(db):
    def __init__(self, id, file=None):
        db.__init__()
        self.id = id
        self.file = file

    def save(self):
        params = (
            'music',
            'file',
            self.file
        )
        self.execute('INSERT INTO %s ( %s ) VALUES ( %s )', params)

#cur.execute("INSERT INTO test (num, data) VALUES (%s, %s)", (100, "abc'def"))

