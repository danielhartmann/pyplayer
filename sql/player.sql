BEGIN;
CREATE TABLE music (
    id serial NOT NULL PRIMARY KEY,
    "file" varchar(100) NOT NULL
);

CREATE TABLE "user" (
    id serial NOT NULL PRIMARY KEY,
    name varchar(100) NOT NULL
);

CREATE TABLE playlist (
    id serial NOT NULL PRIMARY KEY,
    user_id integer NOT NULL REFERENCES "user"(id),
    title varchar(100)
);

CREATE TABLE playlist_music (
    playlist_id integer NOT NULL REFERENCES playlist(id),
    music_id integer NOT NULL REFERENCES music(id),
    stars varchar(5),
    last_played timestamp,
    UNIQUE (playlist_id, music_id)
);

CREATE TABLE preferences (
    config varchar(100) NOT NULL PRIMARY KEY,
    value varchar(100) NOT NULL,
    user_id integer NOT NULL REFERENCES "user"(id)
);

COMMIT;
