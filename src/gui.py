# -*- coding: utf8 -*-

__author__ = "Daniel Hartmann"

import sound

# QT Stuff
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from interface import *
from interface_preferences import Ui_SettingsDialog as SettingsDialog

# configuration file handling
import config

# to control song slider
import threading
import time

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class control(QThread):
    
    running = False

    def __init__(self, ui):
        QThread.__init__(self, ui.MainWindow)
        self.ui = ui

    def run(self):
        while 1:
            pos = self.ui.durationSlider.sliderPosition()
            self.ui.durationSlider.setSliderPosition(pos + 60)
            time.sleep(1)

class playerInterface(Ui_MainWindow):

    def setupUi(self, mw):
        super(playerInterface, self).setupUi(mw)
        self.MainWindow = mw
        self.durationControl = control(self)

    def initPlayer(self):
        self.player = sound.player()

        l = config.config.setdefault('playlist', [])

        rowscount = len(l)
        columnscount = 6 # fixed

        # initialize interface
        self.playlist.setRowCount(rowscount) # FIXME
        self.playlist.setColumnCount(columnscount)

        for x in range(0, rowscount):
            for y in range(0, columnscount):
                self.playlist.setItem(x, y, QtGui.QTableWidgetItem())

        for row, song in enumerate(l):
            songdata = self.player.getMP3Data(song)
            self.playlist.item(row, 0).setText(songdata['tracknumber'])
            self.playlist.item(row, 1).setText(songdata['title'])
            self.playlist.item(row, 2).setText(songdata['artist'])
            self.playlist.item(row, 3).setText(songdata['album'])
            self.playlist.item(row, 4).setText(songdata['year'])
            self.playlist.item(row, 5).setText(song)

        if rowscount > 0:
            self.player.currentsong = l[0]

            length = self.player.getMP3Data(l[0])['length']
            self.durationSlider.setMaximum(int(length*100))
            self.durationSlider.setSliderPosition(0)

            #self.timeLabel.setText('0:00 / ' + str(length))

        # connect play on enter event
        self.playlist.doubleClicked.connect(self.playSelectedRow)


    def openSettingsDialog(self):
        Dialog = QDialog(self.centralwidget)
        self.dialog = SettingsDialog()
        self.dialog.setupUi(Dialog)
        Dialog.show()

    def togglePlaylistVisibility(self):
        if self.playlistWidget.isHidden():
            self.playlistWidget.show()
            height = self.MainWindow.height() + self.playlistWidget.height()
            #self.MainWindow.resize(self.MainWindow.width(), height)
        else:
            height = self.MainWindow.height() - self.playlistWidget.height()
            self.playlistWidget.hide()
            #self.MainWindow.resize(self.MainWindow.width(), height)

    def playerPlay(self):
        
        playerstate = self.player.play()

        if playerstate == self.player.PLAYING:

            if not self.durationControl.isRunning():
                self.durationControl.start()

            pauseIcon = QtGui.QIcon()
            pauseIcon.addPixmap(QtGui.QPixmap(_fromUtf8("../imgs/pause.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            self.playButton.setIcon(pauseIcon)
            self.playButton.setIconSize(QtCore.QSize(30, 30))
            self.playButton.setFlat(True)

            artist = self.player.getMP3Data()['artist']
            self.artistname.setText(_fromUtf8(artist))

            song = self.player.getMP3Data()['title']
            self.songtitle.setText(_fromUtf8(song))

        elif playerstate == self.player.PAUSED:

            self.durationControl.terminate()

            playIcon = QtGui.QIcon()
            playIcon.addPixmap(QtGui.QPixmap(_fromUtf8("../imgs/play.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            self.playButton.setIcon(playIcon)
            self.playButton.setIconSize(QtCore.QSize(30, 30))
            self.playButton.setFlat(True)

    def durationChanged(self, value):
        self.player.changePosition(float(value)/float(100))

    def volumeMuted(self):
        if self.player.volume.get_property('mute'):
            self.player.volume.set_property('mute', False)
            self.volumeLabel.setStyleSheet(_fromUtf8("image: url(\'../imgs/sound.png\')"))
        else:
            self.player.volume.set_property('mute', True)
            self.volumeLabel.setStyleSheet(_fromUtf8("image: url(\'../imgs/mute.png\')"))

    def volumeChanged(self, value):
        volume = float(value)/100
        self.player.changeVolume(volume)

    def playerBand0changed(self, value):
        self.player.bandNChanged(0, value)

    def playerBand1changed(self, value):
        self.player.bandNChanged(1, value)

    def playerBand2changed(self, value):
        self.player.bandNChanged(2, value)

    def addSongDialog(self):
        filedialog = QFileDialog(self.centralwidget)
        filedialog.connect(filedialog, SIGNAL(_fromUtf8("fileSelected(QString)")), self.addSong)
        filedialog.show()

    def addSong(self, path, save=True):
        nextRow = self.playlist.rowCount()
        self.playlist.setRowCount(self.playlist.rowCount() + 1)

        for y in range(0, self.playlist.columnCount()):
            self.playlist.setItem(nextRow, y, QtGui.QTableWidgetItem())

        if save:
            config.addsong(path)

        songdata = self.player.getMP3Data(path)

        self.playlist.item(nextRow, 0).setText(songdata['tracknumber'])
        self.playlist.item(nextRow, 1).setText(songdata['title'])
        self.playlist.item(nextRow, 2).setText(songdata['artist'])
        self.playlist.item(nextRow, 3).setText(songdata['album'])
        self.playlist.item(nextRow, 4).setText(songdata['year'])
        self.playlist.item(nextRow, 5).setText(path)

    def removeSong(self):
        rows = sorted(set([(item.row(), item.text()) for item in self.playlist.selectedItems() if item.column() == 5]), reverse=True)
        for row, song in rows:
            self.playlist.removeRow(row)
            config.removesong(song)
        config.save()

    def playSelectedRow(self):
        items = self.playlist.selectedItems()

        playerstate = self.player.stop()
        self.player.currentsong = items[5].text()
        playerstate = self.player.play()

        if not self.durationControl.isRunning():
            self.durationControl.start()

        pauseIcon = QtGui.QIcon()
        pauseIcon.addPixmap(QtGui.QPixmap(_fromUtf8("../imgs/pause.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.playButton.setIcon(pauseIcon)
        self.playButton.setIconSize(QtCore.QSize(30, 30))
        self.playButton.setFlat(True)

        artist = self.player.getMP3Data()['artist']
        self.artistname.setText(_fromUtf8(artist))

        song = self.player.getMP3Data()['title']
        self.songtitle.setText(_fromUtf8(song))

        self.durationSlider.setMaximum(int(self.player.getMP3Data()['length']*100))
        self.durationSlider.setSliderPosition(0)

    def playNext(self):

        breakNext = False

        for index in range(0, self.playlist.rowCount()):

            i = self.playlist.item(index, 5)

            if i.text() == self.player.currentsong:
                breakNext = True

            elif breakNext:
                playerstate = self.player.stop()
                self.player.currentsong = i.text()
                break

        if not breakNext:
            if self.playlist.item(0, 5):
                playerstate = self.player.stop()
                self.player.currentsong = self.playlist.item(0, 5).text()
            else:
                # No music on playlist
                return

        playerstate = self.player.play()

        if not self.durationControl.isRunning():
            self.durationControl.start()

        pauseIcon = QtGui.QIcon()
        pauseIcon.addPixmap(QtGui.QPixmap(_fromUtf8("../imgs/pause.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.playButton.setIcon(pauseIcon)
        self.playButton.setIconSize(QtCore.QSize(30, 30))
        self.playButton.setFlat(True)

        artist = self.player.getMP3Data()['artist']
        self.artistname.setText(_fromUtf8(artist))

        song = self.player.getMP3Data()['title']
        self.songtitle.setText(_fromUtf8(song))

        self.durationSlider.setMaximum(int(self.player.getMP3Data()['length']*100))
        self.durationSlider.setSliderPosition(0)


    def playPrevious(self):

        breakNext = False

        r = range(0, self.playlist.rowCount())
        r.reverse()
        for index in r:

            i = self.playlist.item(index, 5)

            if i.text() == self.player.currentsong:
                breakNext = True

            elif breakNext:
                playerstate = self.player.stop()
                self.player.currentsong = i.text()
                break

        if not breakNext:
            if self.playlist.item(self.playlist.rowCount()-1, 5):
                playerstate = self.player.stop()
                self.player.currentsong = self.playlist.item(self.playlist.rowCount()-1, 5).text()
            else:
                # No music on playlist
                return

        playerstate = self.player.play()

        if not self.durationControl.isRunning():
            self.durationControl.start()

        pauseIcon = QtGui.QIcon()
        pauseIcon.addPixmap(QtGui.QPixmap(_fromUtf8("../imgs/pause.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.playButton.setIcon(pauseIcon)
        self.playButton.setIconSize(QtCore.QSize(30, 30))
        self.playButton.setFlat(True)

        artist = self.player.getMP3Data()['artist']
        self.artistname.setText(_fromUtf8(artist))

        song = self.player.getMP3Data()['title']
        self.songtitle.setText(_fromUtf8(song))

        self.durationSlider.setMaximum(int(self.player.getMP3Data()['length']*100))
        self.durationSlider.setSliderPosition(0)
