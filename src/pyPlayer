#!/usr/bin/env python

# -*- coding: utf8 -*-

__author__ = "Daniel Hartmann"
__date__ = "$15/03/2011 21:44:22$"

import gui
import sound

# QT Stuff
from PyQt4 import QtCore
from PyQt4 import QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = gui.playerInterface()
    ui.setupUi(MainWindow)

    # initialize the player attribute
    ui.initPlayer()

    # enable mouse events on qlabel
    ui.volumeLabel.setMouseTracking(True)

    # set initial volume slider value
    ui.volumeSlider.setValue(100 * sound.player.INITIAL_VOLUME)
    ui.volumeSlider.setSliderPosition(100 * sound.player.INITIAL_VOLUME)

    # TODO: rotate equalizer labels

    # connect signals between qt class and player interface
    QtCore.QObject.connect(ui.playButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.playerPlay)
    QtCore.QObject.connect(ui.nextButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.playNext)
    QtCore.QObject.connect(ui.previousButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.playPrevious)
    QtCore.QObject.connect(ui.settingsButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.openSettingsDialog)
    QtCore.QObject.connect(ui.showPlaylistButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.togglePlaylistVisibility)

    QtCore.QObject.connect(ui.addButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.addSongDialog)
    QtCore.QObject.connect(ui.removeButton, QtCore.SIGNAL(_fromUtf8("clicked()")), ui.removeSong)

    # connect volume controls
    def volumeEvent(label): ui.volumeMuted()
    ui.volumeLabel.mousePressEvent = volumeEvent
    QtCore.QObject.connect(ui.volumeSlider, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), ui.volumeChanged)

    # connect equalizer sliders
    QtCore.QObject.connect(ui.band0, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), ui.playerBand0changed)
    QtCore.QObject.connect(ui.band1, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), ui.playerBand1changed)
    QtCore.QObject.connect(ui.band2, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), ui.playerBand2changed)

    # connect duration slider
    QtCore.QObject.connect(ui.durationSlider, QtCore.SIGNAL(_fromUtf8("sliderMoved(int)")), ui.durationChanged)

    MainWindow.show()

    # thread to control song position slider
    #c = gui.control(ui)

    sys.exit(app.exec_())
