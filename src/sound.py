# -*- coding: utf8 -*-

__author__ = "Daniel Hartmann"

import collections

# GStreamer Stuff
import pygst
pygst.require("0.10")
import gst

# MP3 ID3 Tag Reading
from mutagen.easyid3 import EasyID3
from mutagen.mp3 import MP3

class player:
    PLAYING = 'playing'
    PAUSED = 'paused'
    STOPPED = 'stopped'

    INITIAL_VOLUME = 0.7

    def __init__(self):
        
        self.currentsong = None
        self.currentposition = 0

        # creates a gstreamer pipeline
        self.pipeline = gst.Pipeline("player-pipeline-dh")

        # sets pipeline properties
        self.filesrc = gst.element_factory_make("filesrc", "audio")
        self.filesrc.set_property("location", self.currentsong)

        # to support more file formats, this must be changed
        self.decoder = gst.element_factory_make("mad", "mp3-decoder")

        self.audioconvert = gst.element_factory_make("audioconvert")
        self.audioresample = gst.element_factory_make("audioresample")

        self.volume = gst.element_factory_make('volume')
        self.volume.set_property('volume', self.INITIAL_VOLUME)
        self.volume.set_property('mute', False)

        # TODO: add some more bands to equalizer
        # equalizers: equalizer-3bands, equalizer-10bands, equalizer-nbands
        self.equalizer = gst.element_factory_make('equalizer-3bands')

        self.sink = gst.element_factory_make("alsasink", "sink")
        self.pipeline.add(
            self.filesrc,
            self.decoder,
            self.audioconvert,
            self.audioresample,
            self.equalizer,
            self.volume,
            self.sink
        )
        gst.element_link_many(
            self.filesrc,
            self.decoder,
            self.audioconvert,
            self.audioresample,
            self.equalizer,
            self.volume,
            self.sink
        )

    def play(self):
        if self.currentsong is None:
            return

        self.filesrc.set_property("location", self.currentsong)
        if self.pipeline.get_state()[1] == gst.STATE_PLAYING:
            self.pipeline.set_state(gst.STATE_PAUSED)
            return self.PAUSED
        else:
            self.pipeline.set_state(gst.STATE_PLAYING)
            return self.PLAYING

    def stop(self):
        self.pipeline.set_state(gst.STATE_READY)
        self.currentposition = 0

    def changeVolume(self, value):
        self.volume.set_property('volume', value)

    def bandNChanged(self, bandNumber, value):
        self.equalizer.set_property( "band"+str(bandNumber), float( value ) )

    def getMP3Data(self, songname=None):
        if not songname:
            songname = self.currentsong

        # id3 data
        song = EasyID3(songname)
        # audio data
        audio = MP3(songname)

        songdata = collections.defaultdict(lambda: '')
        for field in song:
            songdata[field] = song[field][0]

        songdata['bitrate'] = audio.info.bitrate
        songdata['length'] = audio.info.length

        return songdata

    def changePosition(self, value):
        self.pipeline.seek_simple(gst.FORMAT_TIME, gst.SEEK_FLAG_FLUSH | gst.SEEK_FLAG_KEY_UNIT, value * gst.SECOND)
        self.currentposition = value

